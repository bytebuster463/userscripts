## Important note: Hand-edited changes to this layer have a high risk of 
## being erased the next time you use the wizard. Setting properties is 
## usually safe, but if you put entire functions, they *will* be overwritten.

layerinfo "type" = "user";
layerinfo "name" = "Auto-generated Customizations";
set comment_date_format = "long_day_dayfirst";
set comment_time_format = "short_24";
set custom_css = "#title,
#pagetitle,
#subtitle,
.entry-title,
.module-header,
.comment-title,
.tags-container h2,
#archive-year .month .header h3 {
    text-transform: none;
}
.entry-content li,
.comment-content li {
    list-style-position: outside;
    margin-left: 10px;
}
blockquote {
    border-left: 6px double #887766;
    margin: 1em 1em .5em;
    padding: .5em .75em;
    }";
set entry_date_format = "long_day_dayfirst";
set entry_time_format = "short_24";
set font_base_size = "80";	
set font_comment_title_units = "em";
set font_entry_title_units = "em";
set font_journal_subtitle_units = "em";
set font_journal_title_units = "em";
set font_module_heading_units = "em";
set font_module_text_units = "em";
set module_calendar_order = 23;
set module_calendar_section = "two";
set module_calendar_show = false;
set module_credit_order = 25;
set module_credit_section = "two";
set module_customtext_show = true;
set module_navlinks_order = 22;
set module_navlinks_section_override = "none";
set module_navlinks_show = false;
set module_pagesummary_section = "none";
set module_pagesummary_show = false;
set module_poweredby_order = 24;
set module_time_order = 21;
set reg_firstdayofweek = "monday";

##############################
## Localization begins here
##############################

set text_admin_post_subject = "Керувати:";
set text_base_layout_authors = "Основний стиль:";
set text_calendar_num_entries = "1 допис // # дописів";
set text_cuttagcontrols_nocuttags = "Без прихованого";
set text_day_next = "Наступний день";
set text_day_prev = "Попередній день";
set text_deleted = "(видалено)";

set text_comment_date = "Дата:";
set text_comment_edittime = "Змінено";
set text_comment_expand = "Розгорнути";
set text_comment_from = "Від:";
set text_comment_frozen = "Заморожено";
set text_comment_hide = "Сховати 1 коментар // Сховати # коментарів";
set text_comment_ipaddr = "IP адреса:";
set text_comment_link = "Посилання";
set text_comment_parent = "Первинний коментар";
set text_comment_posted = "Коментар успішно додано.";
set text_comment_reply = "Відповісти на коментар";
set text_comment_thread = "Гілка";
set text_comment_threadroot = "Уся гілка";
set text_comment_unhide = "Показати 1 коментар // Показати # коментарів";

set text_comments_disabled_maintainer = "Коментарі заборонені керівником спільноти";

set text_commentview_flat = "Списком";
set text_commentview_threaded = "Деревом";
set text_commentview_toponly = "Лише первинні коментарі";

set text_edit_comment_title = "Редагувати відповідь";
set text_edit_entry = "Редагувати";
set text_edit_tags = "Редагувати мітки";

set text_entry_next = "Уперед";
set text_entry_prev = "Назад";

set text_fromsuspended = "(коментар від заблокованого користувача)";
set text_frozen = "(Заморожено)";
set text_generated_on = "Сторінку створено";
set text_icon_manage = "Керувати аватарами";
set text_icons_comment = "Коментар:";
set text_icons_default = "Основний";
set text_icons_description = "Опис:";
set text_icons_inactive = "Неактивний";
set text_icons_keywords = "Мітки:";
set text_icons_page_empty_header = "Немає аватарів";
set text_icons_page_header = "Ваші аватари";
set text_icons_sort_keyword = "За ключовими словами";
set text_icons_sort_upload = "За датою завантаження";
set text_layout_authors = "Стиль:";
set text_layout_resources= "Ресурс:";
set text_linklist_manage = "Керувати посиланнями";

set text_max_comments = "Забагато коментарів";
set text_mem_add = "Запам'ятати";

set text_meta_groups = "Групи доступу:";
set text_meta_location = "Де я:";
set text_meta_mood = "Настрій:";
set text_meta_music = "Слухаю:";
set text_meta_xpost = "Перепощено до:";

set text_module_active_entries = "Свіжі дописи";
set text_module_credit = "За стиль дякувати";
set text_module_customtext = "Додатковий текст";
# set text_module_customtext_content = "(порожньо)";
# set text_module_customtext_url = "";
set text_module_cuttagcontrols = "Розгорнути cut";
set text_module_links = "Посилання";
set text_module_pagesummary = "Дописи";
set text_module_popular_tags = "Популярні мітки";
set text_module_search = "Пошук";
set text_module_search_btn = "Пошук";
set text_module_subscriptionfilters = "Фільтри підписок";
set text_module_syndicate = "RSS";
set text_module_tags = "Мітки";
set text_module_userprofile = "Про блоґ";

set text_month_form_btn = "Дивитися";
set text_month_screened_comments = "Приховані";
set text_multiform_btn = "Виконати дію";
set text_multiform_check = "Обрати:";
set text_multiform_conf_delete = "Видалити обрані коментарі?";
set text_multiform_des = "Групова дія над обраними коментарями:";
set text_multiform_opt_delete = "Видалити";
set text_multiform_opt_deletespam = "Видалити як спам";
set text_multiform_opt_edit = "Редагувати";
set text_multiform_opt_freeze = "Заморозити";
set text_multiform_opt_screen = "Приховати";
set text_multiform_opt_track = "Стежити";
set text_multiform_opt_unfreeze = "Розморозити";
set text_multiform_opt_unscreen = "Показати";
set text_multiform_opt_unscreen_to_reply = "Показати, щоб відповісти";
set text_multiform_opt_untrack = "Не стежити";
set text_new_comment_title = "Відповісти";
set text_noentries_day = "Не було дописів цієї доби.";
set text_noentries_read = "Немає більш ранніх дописів. Ця сторінка показує останні 1000 дописів за останні 14 днів.";
set text_noentries_recent = "Немає жодного допису";
set text_nosubject = "...";
set text_nosubject_screenreader = "...";
set text_openid_from = "від";
set text_page_top = "Наверх";


set text_permalink = "Посилання";
set text_post_comment = "Коментувати допис";
set text_post_comment_friends = "Відповісти";
set text_poster_anonymous = "(Анонімно)";
set text_posting_in = "надіслано до";

set text_powered_by = "Створено з";

set text_read_comments = "1 коментар // # коментарів";
set text_read_comments_friends = "1 коментар // # коментарів";
set text_read_comments_screened = "1 схований // # схованих";
set text_read_comments_screened_visible = "1 видимий // # видимих";
set text_read_comments_threads = "1 відповідь // # відповідей";

set text_reply_back = "Читати коментарі";
set text_replyform_header = "Коментар від";
set text_reply_nocomments = "Для цього допису коментарі заборонені.";
set text_reply_nocomments_header = "Коментарі заборонені:";
set text_screened = "(прихований коментар)";

set text_skiplinks_back = "Попередні #";
set text_skiplinks_forward = "Наступні #";
set text_stickyentry_subject = "Важливо:";
set text_subscriptionfilters_manage = "Керувати фільтрами";

set text_tags = "Мітки:";
set text_tags_manage = "Керувати мітками";
set text_tags_page_header = "Видимі мітки";
set text_tag_uses = "1 раз // # разів";

set text_tell_friend = "Поділитися";
set text_theme_authors = "Тема:";
set text_unwatch_comments = "Не слідкувати";

set text_view_archive = "Архів";
set text_view_day = "День";
set text_view_entry = "Допис";
set text_view_friends = "Читати";
set text_view_friends_comm = "Дописи спільноти";
set text_view_friends_filter = "Читати (фільтр)";
set text_view_icons = "Аватари";
set text_view_memories = "Збережені";
set text_view_month = "Дивитися заголовки";
set text_view_monthpage = "Місяць";
set text_view_network = "Мережа";
set text_view_network_filter = "Мережа (фільтр)";
set text_view_recent = "Нове";
set text_view_recent_tagged = "Дописи з міткою";
set text_view_tags = "Мітки";
set text_view_userinfo = "Про блоґ";
set text_watch_comments = "Слідкувати";

set text_watch_comments = "Слідкувати";
set text_website_default_name = "Мій веб сайт";
set time_ago_days = "1 день // # днів";
set time_ago_hours = "1 година // # годин";
set time_ago_minutes = "1 хвилина // # хвилин";
set time_ago_seconds = "1 секунда // # секунд";

set lang_dayname_long = [

"",
"Неділя",
"Понеділок",
"Вівторок",
"Середа",
"Четвер",
"П'ятниця",
"Субота"
];

set lang_dayname_short = [

"",
"Нед",
"Пон",
"Вів",
"Сер",
"Чет",
"П'ят",
"Суб"
];

set lang_dayname_shorter =[
"",
"Н",
"П",
"В",
"С",
"Ч",
"П",
"С"
];

set lang_monthname_long =[

"",
"Січень",
"Лютий",
"Березень",
"Квітень",
"Травень",
"Червень",
"Липень",
"Серпень",
"Вересень",
"Жовтень",
"Листопад",
"Грудень"

];

set lang_monthname_short = [

"",
"Січ",
"Лют",
"Бер",
"Кві",
"Тра",
"Чер",
"Лип",
"Сер",
"Вер",
"Жов",
"Лис",
"Гру"
];
