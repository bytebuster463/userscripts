Ukrainian dictionary for Semagic
===================

This directory contains a modified OpenOffice/MySpell dictionary for Ukrainian,
suitable for use with [Semagic](http://semagic.sourceforge.net/index.html).

The original dowloaded from [openoffice.org](http://www.artfiles.org/openoffice.org/contrib/dictionaries/)

The only difference that it was converted from UTF-8 to CP-1251 which Semagic seems to "understand" well

Converted by http://bytebuster.dreamwidth.org

=================

Цей каталог містить змінений словник  OpenOffice/MySpell для української мови,
який здатен працювати із [Semagic](http://semagic.sourceforge.net/index.html).

Оригінальна версія скачана з [openoffice.org](http://www.artfiles.org/openoffice.org/contrib/dictionaries/)

Єдина зміна — це що цей словние був конвертований із UTF-8 до CP-1251, з яким Semagic може нормально  "порозумітися".

Converted by http://bytebuster.dreamwidth.org
