// ==UserScript==
// @name        StackExchange Bad Voters
// @namespace   bytebuster.dreamwidth.org
// @description Shows the user's vote/reputation ratio
// @include     /https?://.*ukrainian\.stackexchange\.com/.*/
// @version     0.2
// @require       https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js
// @grant       GM.getValue
// @grant       GM.setValue
// @grant       GM.deleteValue
// ==/UserScript==

// fix include 

/* **********************************************************************************

    This script has two main functions:

********************************************************************************** */

/* ************* VERSION HISTORY ****************************************************
 * 0.2 Migrated to GM 4.0, made async workout
 * 0.1 Initial reease
*/

/* ************* TODO ***************************************************************

* (none yet)

*/

(function(){
  // **** SETTINGS ****************************************************
  const DEBUG = false;

  // **** CONSTANTS ***************************************************
  // You don't need it unless DreamWidth changes its classes (impossible, yeah)

  const RATES = {
    20: "red",
    10: "grey",
    0:  "green"
  };
  const RATES_DATA_TAG = "RATES_CACHE";

  function getRateColor(ratio) {
    var ret = ""; // need this because array is auto-sorted
    for(var key in RATES) {
      if(ratio > parseInt(key)) {
        ret = RATES[key];
      }
      //alert("ratio="+ratio+", ask="+key+", now="+ret);
    };
    return ret;
  }
  function addSpinner(element) {

  }
  function makeVoteSpan(voteScore, repScore) {
    const s=0;
    var voteRatio = (repScore / voteScore).toFixed(1);
    var vs =
        $("<span />", {
          class: 'reputation-score',
          title: 'rep/votes ratio '+voteRatio+'='+repScore+"/"+voteScore,
          style: 'color: '+getRateColor(voteRatio),
          text: '|' + voteRatio.toString()
        });
    return vs;
  }

  async function populateVotes() {
    var userId;
    var votesString = await GM.getValue(RATES_DATA_TAG, "{}");
    var votesAll = JSON.parse(votesString);

    var isTentative; // TODO incomplete

    $(".user-info .user-details").each(function() {
      addSpinner($(this));
      // alert("this.outerHTML="+this.outerHTML);
      var userAnchor = $(this).find("a").first().attr("href");
      if(! userAnchor) {return;} // skip own post edits, no userinfo there

      var matches = userAnchor.match(/\/users\/(\d+)\/.*/);
      // alert("matches="+matches);
      if(! matches) {return;} // comm wiki
      userId = matches[1];
      isTentative = false;
      var oneUserData = votesAll[userId];
      if(oneUserData === undefined) {return;}
      // TODO isTentative=true; votes=10; rep=not populated???
/*
        {
          oneUserData = {
            rep:   1000,
            votes: 30,
          };
        }
*/
      //alert(oneUserData+ "/"+ (oneUserData === undefined) + "/"+JSON.stringify(oneUserData));
      var voteSpan = makeVoteSpan(oneUserData.votes, oneUserData.rep);

      $(voteSpan).insertAfter($(this).find(".reputation-score"))
      // alert(userName);
    })
  }
  async function rememberVotes() {
    var userRep;
    var votesString = await GM.getValue(RATES_DATA_TAG, "{}");
    var votesAll = JSON.parse(votesString);
    //alert("old: "+ JSON.stringify(votesAll));

    $(".user-info").each(function(){
      var userId = $(this).find(".user-details a").attr("href").match(/\/users\/(\d+)\/.*/)[1];
      var userRep1 = $(this).find(".reputation-score").attr("title").match(/[\d,]+/g);
      var userRep2 = $(this).find(".reputation-score").text().match(/[\d,]+/g);
      if(userRep1 !== null) {
        userRep = userRep1[0].replace(',','');
      } else {
        userRep = userRep2[0].replace(',','');
      }
      var userVotes = $(this).find(".user-tags").text().match(/([\d,]+) votes/)[1];
      //alert("id="+userId+", rep="+userRep+", votes="+userVotes);
      var oneUserData = {
        rep:   userRep,
        votes: userVotes,
      };
      votesAll[userId] = oneUserData;
    });

    // TODO: Write a notification on a page to say, "me updated"
    //alert(JSON.stringify(votesAll));
    await GM.setValue(RATES_DATA_TAG, JSON.stringify(votesAll));

    // Update window
    $("<div />", {
      class: '',
      style: 'background-color: #ff8888; color: yellow',
      text: 'Data updated'
    }).insertAfter($(".subheader"));

  }

  const VOTERS_REGEX = /\/users\?.*tab=Voters&filter=all/i;
  var thisUrl = window.location.href;
  if(VOTERS_REGEX.test(thisUrl)) {
    // alert("rememberVotes");
    rememberVotes();
    populateVotes();
  } else {
    //alert("populateVotes");
    populateVotes();
  }
})();