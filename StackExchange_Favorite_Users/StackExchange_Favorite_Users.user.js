// ==UserScript==
// @name        StackExchange Favorite Users
// @namespace   bytebuster.dreamwidth.org
// @description Highlights posts by favorite users
// @include     /https?:\/\/.*(stackoverflow|superuser|serverfault|stackapps|mathoverflow|askubuntu|stackapps|stackexchange)\.com\/.*/
//
// @version     0.2
// @require       https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js
// @grant       GM.getValue
// @grant       GM.setValue
// @grant       GM.deleteValue
// ==/UserScript==

/* **********************************************************************************

    This script has two main functions:

********************************************************************************** */

/* ************* VERSION HISTORY ****************************************************
 * 0.1 Initial reease
 * 0.2 added tentative search
*/

/* ************* TODO ***************************************************************

* (none yet)

*/

(function() {

  // **** SETTINGS ****************************************************
  const DEBUG = false;

  const GROUPS = {
    1: { name: "Muscovy", color: "sienna" },
    2: { name: "Dunno", color: "lightsalmon" },
    3: { name: "MaybeFriends", color: "greenyellow" },
    4: { name: "Friends", color: "lightgreen" }
  };

  // Contains comma-separated usernames for each fav group
	// TODO eliminate after UI for adding users to groups is done
  const FAV_DATA_RAW = {
    // Muscovy
    1: ", ",
    // Dunno
    2: ", ",
    // MaybeFriends
    3: ", ",
    // Friends
    4: "bytebuster, bytebuster for Long Usernames, politics@2984, "
  };

  
  // **** CONSTANTS ***************************************************
  // You don't need it unless DreamWidth changes its classes (impossible, yeah)

  const STYLE_USERGROUP = `
		.userG%number% {
				/*border-radius:15px;*/
				border-left: solid %color% 8px;
  			border-image: 
    			linear-gradient(
      			to left, 
      			%color%,
      			rgba(0, 0, 0, 0)
    			) 1 100%;
				transition: border 500ms ease 1s;
    }

		.userG%number%-maybe {
				border-left: dotted %color% 5px;
				transition: border 500ms ease 1s;
    }
	`;
/*
		.userG%number% {
				box-shadow: -6px 0px 5px -3px %color%;
				transition: box-shadow 500ms ease 1s;
    }
    .userG%number%-maybe {
				box-shadow: -6px 0px 5px -3px %color%;
				outline: dotted 5px white;
				transition: box-shadow 500ms ease 1s;
    }

		linear-gradient(%color%,white) left/10px   100% no-repeat, #f2f2f2;
 		border: solid 15px white;
		padding-left:5px !important;

*/
  

  const STYLE_INFOPANEL = `
		textarea {
			width: 100%;
      margin:0;
      padding:0;
    	-webkit-box-sizing:border-box;
    	-moz-box-sizing:border-box;
    	box-sizing:border-box;
    }

		.favusers.spinner {
      border-left: 2px solid rgba(129,27,26,0.3) !important;
    }
   	.favuser-info {
				cursor:pointer;
				display:inline;
        float:right;
				width:16px;
				height:16px;
		}
		.favuser-info.info-absent {
				background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 65 65'><g><g><path d='M32.5,0C14.58,0,0,14.579,0,32.5S14.58,65,32.5,65S65,50.421,65,32.5S50.42,0,32.5,0z M32.5,61C16.785,61,4,48.215,4,32.5 S16.785,4,32.5,4S61,16.785,61,32.5S48.215,61,32.5,61z'/><circle cx='33.018' cy='19.541' r='3.345'/><path d='M32.137,28.342c-1.104,0-2,0.896-2,2v17c0,1.104,0.896,2,2,2s2-0.896,2-2v-17C34.137,29.237,33.241,28.342,32.137,28.342z'/></g></g></svg>");
		}
		.favuser-info.info-present {
				background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 46 46'><g><g><path d='M39.264,6.736c-8.982-8.981-23.545-8.982-32.528,0c-8.982,8.982-8.981,23.545,0,32.528c8.982,8.98,23.545,8.981,32.528,0 C48.245,30.281,48.244,15.719,39.264,6.736z M25.999,33c0,1.657-1.343,3-3,3s-3-1.343-3-3V21c0-1.657,1.343-3,3-3s3,1.343,3,3V33z M22.946,15.872c-1.728,0-2.88-1.224-2.844-2.735c-0.036-1.584,1.116-2.771,2.879-2.771c1.764,0,2.88,1.188,2.917,2.771 C25.897,14.648,24.746,15.872,22.946,15.872z'/></g></g></svg>");
    }
		.usergroups {
			overflow:hidden;
			border-left: 1px solid grey;
			padding-left: 5px;
		}
		.usergroups-select {
			width:200px; height:400px; border:none; overflow:hidden;
			padding-left:6px;
		}
		.usergroup-option {
			font-size: 14pt;
		}
		.usercomment {
			padding:5px;
		}
		.favuser-comment {
			font-size: 12pt;
			margin:0 0 7px 0 !important;
		}
    
    .auto-review-comments.popup {
      position:absolute;
      display:block;
      width:690px;
      padding:15px 15px 10px
    }
    .auto-review-comments.popup .float-left{float:left}
    .auto-review-comments.popup .float-right{float:right}
    .auto-review-comments.popup .throbber{display:none}
    .auto-review-comments.popup .remoteerror{color:red}
    .auto-review-comments.popup>div>textarea{width:100%;height:442px}
    .auto-review-comments.popup .main{overflow:hidden}
    .auto-review-comments.popup .main .userinfo{padding:5px;margin-bottom:7px;background:#eaefef}
    .auto-review-comments.popup .main .action-list {
      height:440px;margin:0 0 7px 0 !important;
      overflow-y:auto
    }
    .auto-review-comments.popup .main .action-list li{width:100%;padding:0;transition:.1s}
    .auto-review-comments.popup .main .action-list li:hover{background-color:#f2f2f2}
    .auto-review-comments.popup .main .action-list li.action-selected:hover{background-color:#e6e6e6}
    .auto-review-comments.popup .main .action-list li input{display:none}
    .auto-review-comments.popup .main .action-list li label{position:relative;display:block;padding:10px}
    .auto-review-comments.popup .main .action-list li label .action-name{display:block;margin-bottom:3px;cursor:default}
    .auto-review-comments.popup .main .action-list li label .action-desc{margin:0;color:#888;cursor:default}
    .auto-review-comments.popup .main .action-list li label .action-name textarea,.auto-review-comments.popup .main .action-list li label .action-desc textarea{width:99%;margin:0 0 -4px 0}
    .auto-review-comments.popup .main .action-list li label .action-desc textarea{height:42px}
    .auto-review-comments.popup .main .action-list li label .quick-insert{display:none;position:absolute;top:0;right:0;height:100%;margin:0;font-size:300%;color:transparent;border:0;transition:.3s;text-shadow:0 0 1px #fff;cursor:pointer;background-color:rgba(0,0,0,0.1);background:rgba(0,0,0,0.1);box-shadow:none;-moz-box-shadow:none;-webkit-box-shadow:none}
    .auto-review-comments.popup .main .action-list li:hover label .quick-insert{display:block}.auto-review-comments.popup .main .action-list li label .quick-insert:hover{background-color:#222;color:#fff}
    .auto-review-comments.popup .main .share-tip{display:none}
    .auto-review-comments.popup .main .share-tip .customwelcome{width:300px}
    .auto-review-comments.popup .main .share-tip .remoteurl{display:block;width:400px}
    .auto-review-comments.popup .actions,.auto-review-comments.popup .main .popup-actions .actions{margin:6px}
    .auto-review-comments.popup .main .popup-actions .popup-submit{float:none;margin:0 0 5px 0}
		.auto-review-comments.announcement{padding:7px;margin-bottom:10px;background:orange;font-size:15px}
    .auto-review-comments.announcement .notify-close{display:block;float:right;margin:0 4px;padding:0 4px;border:2px solid black;cursor:pointer;line-height:17px}
    .auto-review-comments.announcement .notify-close a{color:black;text-decoration:none;font-weight:bold;font-size:16px}
    .auto-review-comments.popup .main .searchbox{display:none}
    .auto-review-comments.popup .main .searchfilter{width:100%;box-sizing:border-box;display:block}
	`;

  const POPUP_DIALOG = `
		<div class="auto-review-comments popup" id="popup">
      <div class="popup-close" id="close"><a title="close this popup (or hit Esc)">&#215;</a></div>
      <h2 class="handle">User Information</h2>
      <div class="main" id="main">
        <div class="popup-active-pane">
          <div class="userinfo" id="userinfo"> <img src="//sstatic.net/img/progress-dots.gif"/> </div>
          <div class="searchbox"> <input type="search" class="searchfilter" placeholder="filter the comments list"/></div>
					<div class="usergroups action-list float-right">
						<h3 class="poptitle3">Highlight Groups</h3>
						<select class="usergroups-select" multiple>
  						<option value="volvo">Volvo</option>
  						<option value="saab">Saab</option>
  						<option value="opel">Opel</option>
  						<option value="audi">Audi</option>
						</select> 
					</div>
					<div class="usercomment action-list">
						<h3 class="poptitle3">Comments</h3>
						<textarea name="user-comment" class="favuser-comment" cols="80" rows="5"></textarea>
					</div>
          <!--ul class="action-list"> </ul -->
        </div>
        <div class="share-tip" id="remote-popup"> enter url for remote source of comments (use import/export to create jsonp) <input class="remoteurl" id="remoteurl" type="text"/> <img class="throbber" id="throbber1" src="//sstatic.net/img/progress-dots.gif"/> <span class="remoteerror" id="remoteerror1"></span> <div class="float-left"> <input type="checkbox" id="remoteauto"/> <label title="get from remote on every page refresh" for="remoteauto">auto-get</label> </div> <div class="float-right"> <a class="remote-get">get now</a> <span class="lsep"> | </span> <a class="remote-save">save</a> <span class="lsep"> | </span> <a class="remote-cancel">cancel</a> </div> </div>
        <div class="share-tip" id="welcome-popup"> configure "welcome" message (empty=none): <div> <input class="customwelcome" id="customwelcome" type="text"/> </div> <div class="float-right"> <a class="welcome-force">force</a> <span class="lsep"> | </span> <a class="welcome-save">save</a> <span class="lsep"> | </span> <a class="welcome-cancel">cancel</a> </div> </div>
        <div class="popup-actions">
          <div class="float-left actions">
            <a title="close this popup (or hit Esc)" class="popup-actions-cancel">cancel</a> <span class="lsep"> | </span>
            <a title="see info about this popup (v1.4.4)" class="popup-actions-help" href="//github.com/Benjol/SE-AutoReviewComments" target="_blank">info</a> <span class="lsep"> | </span>
            <a class="popup-actions-see">see-through</a> <span class="lsep"> | </span>
            <a title="use this to import/export favorite users" class="popup-actions-impexp">import/export</a> <span class="lsep"> | </span>
          </div>
          <div class="float-right"> <input class="popup-submit" type="button" disabled="disabled" value="Save"> </div>
        </div>
      </div>
    </div>
	`;

  // Name of GM-stored global variable with data
  const FAV_DATA_TAG = "FAV_CACHE";


  // **** GLOBAL VARS ***************************************************
  // A global var for all known fav users data
  var favData;

  /* ************ SERVER-SIDE  ************
   * Stores JSON data and uses GreaseMonkey's engine to load/save
  */

  document.addEventListener ("foozzz", function (e) {
		//browser.runtime.onMessage.addListener(e => {
	  //console.log("GM.getValue="+ GM.getValue);
//		console.log("foo-inner " + JSON.stringify(e.detail));
	  //sendResponse({response: "Response from background script"});
		//}, false);
	});

  // console.log("foo1-1");
  // Create the event
	var event = new CustomEvent("foozzz", { "detail": "Example1 of an event" });
	// Dispatch/Trigger/Fire the event
	document.dispatchEvent(event);


  /* */
  function with_jquery(data, f) {

    var script = document.createElement("script");
    script.type = "text/javascript";
    script.textContent += "DATA="+JSON.stringify(data)+";";
    //console.log(`data= ${script.textContent}`);
    script.textContent += "(" + f.toString() + ")(jQuery)";
    document.body.appendChild(script);
  };


//  async function ReadFavs() {

      // Read stored fav users data
//    var favString = "{}"; //await GM.getValue(FAV_DATA_TAG, "{}");
    GM.getValue(FAV_DATA_TAG, "[]").then(favString => {
    favData = JSON.parse(favString);
    //console.log(`B6 favData= ${JSON.stringify(favData)}`);

    // Append the hard-coded string data
    for (var key in FAV_DATA_RAW) {
      //dalert("3: "+FAV_DATA_RAW[key]);
      FAV_DATA_RAW[key].split(',').forEach(name0 => {
        //dalert("4: "+name0);
        //console.log(`B7 favData= ${JSON.stringify(favData)}`);
        var name = name0.trim().toLowerCase();
        if(name != "") {
        	favData.push( { "name": name, group: key });
        };
      });
    };
    //console.log(`ReadFavs returning: ${JSON.stringify(favData)}`);
//  }

  //ReadFavs();

/*
  browser.runtime.sendMessage({
    greeting: "Greeting from the content script"
  })
    .then(
    (message => {
  		console.log(`Message from the background script1:  ${message}`);
		}),
    (error => {
  		console.log(`Error1: ${error}`);
		})
	);
*/
  //console.log("foo1-2");

  	//console.log(`BRK2 favData= ${JSON.stringify(favData)}`);

var DATA = {
  groups: GROUPS,
  favData: favData,
  STYLE_USERGROUP: STYLE_USERGROUP,
  STYLE_INFOPANEL: STYLE_INFOPANEL,
  POPUP_DIALOG: POPUP_DIALOG,
};

/* ***********************************************
*** Push the following to client side
*** so thet the script could use the Stack Exchange native scripts
*** Also, push all global vars to the client scope
************************************************* */

with_jquery(DATA, client);
    });

var client = function ($) {
  StackExchange.ready(function () {
/* */

  	//console.log(`BRK3 favData= ${JSON.stringify(favData)}`);
  	//console.log(`INNER data= ${JSON.stringify(DATA)}`);
    var GROUPS = DATA.groups;
    var favData = DATA.favData;
		const STYLE_USERGROUP = DATA.STYLE_USERGROUP;
  	const STYLE_INFOPANEL = DATA.STYLE_INFOPANEL;
  	const POPUP_DIALOG = DATA.POPUP_DIALOG;

   	//console.log(`BRK4 favData= ${JSON.stringify(favData)}`);

/*    
(function(StackExchange) {
*/

	  const FAVDEFAULT = { group: 0, certain: 1 };
  	function getUserGroup(userSite, userId, userName) {
	    // TODO: add functionality to process users by ID, not only by name
      var userNameLower = userName.toLowerCase();
      //console.log(`userNameLower="${userNameLower}"`);
			//console.log(`typeof=` + favData.getOwnPropertyNames() );

      // console.log(`userSite="${userSite}"`);
      var x1 = userSite+"@"+userId;
      //console.log(`looking for="${x1}"`);
	    var ret =
          favData.find(fav => fav.name == x1) ||
          favData.find(fav => userNameLower == fav.name);
      if(ret !== undefined) {
        ret.certain = 9;
      } else {
      	ret =
          favData.find(fav => userNameLower.startsWith(fav.name)); //|| FAVDEFAULT;
	      if(ret !== undefined) {
  	      ret.certain = 5;
    	  } else {
        	ret = FAVDEFAULT;
        }
      }
      //console.log(`ret=${JSON.stringify(ret)}`);
	    if (userName == "bytebuster") { ret.info = "foobar"; }
	    return ret;
	  }

    function saveData(userSite, userId, userName, userData) {
//   		console.log("saveData for %o: %o", userName, JSON.stringify(userData));

//       console.log("old data=%o", JSON.stringify(favData[userName.toLowerCase()]));
      favData[userName.toLowerCase()] = userData;
//       console.log("new data=%o", JSON.stringify(favData[userName.toLowerCase()]));
      
  		// Create the event
			var event = new CustomEvent("foozzz", userData);

			// Dispatch/Trigger/Fire the event
			document.dispatchEvent(event);

    }
    
    saveData("", "", "", { "detail": "Example2 of an event" });
   
    /*
      browser.runtime.sendMessage({
    greeting: "Greeting from the content script"
  })
    .then(
    (message => {
  		console.log(`Message from the background script1:  ${message}`);
		}),
    (error => {
  		console.log(`Error1: ${error}`);
		})
	);
*/  
//  console.log("foo2-2");

  // **** CONSTANTS ***************************************************
  // You don't need to change it


  // A global var to list all styles e.g. "userG1 userG2 ..."
  var allGroups;


  function DebugMessage(what) {
    if (DEBUG) { console.log(what); }
  }
  function dalert(what) {
    if (DEBUG) { alert(what); }
  }
  function addSpinner(element, isSet = true) {
    if (isSet) {
      $("<div class='favusers spinner'/>").insertAfter($(element).find(".user-details a"));

    } else {
      var spinner = $(element).find(".spinner");
      if (spinner.length) { $(spinner).remove(); }
    }
  }

  function addGlobalStyle(css) {
    // TODO make it shorter
    // var cssElement = $(cssTemplate);
    // $("head").append(cssElement);    

    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
  }

  async function highlightPosts() {
    //var votesString = await GM.getValue(RATES_DATA_TAG, "{}");
    //var votesAll = JSON.parse(votesString);
    var userSite = window.location.host.match(/[^\.]+/);

    var isTentative; // TODO incomplete

    // Read stored fav users data
    // TODO CHECK
    
    $(".user-info").each(function (i, userInfo) {
      var userLink, userId, userName;
      var post;
      addSpinner(userInfo);

      userLink = $(userInfo).find(".user-details a")[0];
      //dalert("user1= "+$(userLink).html());
      if (userLink) {
        var matches = userLink.href.match(/\/users\/([-\d]+)\//);
        if (matches) {
          userId = matches[1];
          userName = userLink.innerText;
          //dalert(userId +"/"+ userName +"/"+ userSite);
          var favUserInfo = getUserGroup(userSite, userId, userName);
          // console.log(`favUserInfo= ${JSON.stringify(favUserInfo)}`);
          var newClassId = favUserInfo.group || 0;
          var newClassMaybe = (favUserInfo.certain < 9 ? "-maybe" : "");
          //dalert("class="+newClassId);

          // Add more info on User
          if(! $(userInfo).find(".favuser-info").length ) {
          	var elFavUserInfo = "<div class='favuser-info %info%' data-userid='%userid%' data-username='%username%' %title%></div>";
          	// dalert("favUserInfo.info=["+favUserInfo.info+"]");
          	var replacements = {
	            "%info%": (favUserInfo.info === undefined ? "info-absent" : "info-present"),
	            "%title%": (favUserInfo.info === undefined ? " " : "title='" + favUserInfo.info + "'"),
              "%userid%": userId,
              "%username%": userName,
	          };
	          elFavUserInfo = elFavUserInfo.replace(/%\w+%/g, function (placeholder) {
	            return replacements[placeholder] || placeholder;
	          });
	          $(elFavUserInfo).insertBefore($(userInfo).find(".user-details .-flair"))
					}

          if (newClassId != 0) {
            var newClass = "userG" + newClassId + newClassMaybe;
            //console.log(`newClass= ${newClass}`);

            // Highlight user
            $(userInfo).addClass(newClass);

            // dalert($(userInfo).parent().next().find(".user-info").html());
            if ($(userInfo).parent().next().find(".user-info").length == 0) {
              // Highlight post if possible
              post = $(userInfo).closest(".question,.answer");
              if (post) {
                $(post).removeClass(allGroups).addClass(newClass);
              }
            }
          }
        }
      }
      addSpinner(userInfo, false);
    });
    
		function PopulateUserGroups(popup) {
      // if(!GetStorage("commentcount")) ResetComments();
      const ONE_GROUP = `<option value="%number%" class="userG%number% usergroup-option">%name%</option>`;
			var ul = popup.find('.usergroups-select');
			ul.empty();
  		for (var key in GROUPS) {
    		var replacements = { "%number%": parseInt(key), "%color%": GROUPS[key].color, "%name%": GROUPS[key].name, };
    		var oneGroup = ONE_GROUP.replace(/%\w+%/g, function (placeholder) {
      		return replacements[placeholder] || placeholder;
    		});
        ul.append(oneGroup);
		  };
    }

    function popup(el) {
			var popup = $(POPUP_DIALOG);
			popup.find('.popup-close').click(function () { popup.fadeOutAndRemove(); });
      //Add handlers for command links
      popup.find('.popup-actions-cancel').click(function () { popup.fadeOutAndRemove(); });
      popup.find('.popup-actions-see').hover(function () {
        popup.fadeTo('fast', '0.4').children().not('#close').fadeTo('fast', '0.0')
      }, function () {
        popup.fadeTo('fast', '1.0').children().not('#close').fadeTo('fast', '1.0')
      });

      popup.find('.usergroups-select').change(function () {
        popup.find('.popup-submit').removeAttr("disabled"); //enable submit button
      });
      popup.find('.favuser-comment').keyup(function () {
        popup.find('.popup-submit').removeAttr("disabled"); //enable submit button
      });

			//alert(1);
			$("#content").append(popup);
      //alert(2);
      PopulateUserGroups(popup);

      var userId = el.data("userid");
      var userName = el.data("username");
      var favUserInfo = getUserGroup(userSite, userId, userName); // TODO refactor to avoid 2nd call
//       console.log("z1 %o", favUserInfo);
      
//       console.log("z21 %o", popup.find('.favuser-comment')[0]);
			//var userGroupId = favUserInfo.group || 0;
//       console.log("z22 %o", popup.find('.favuser-comment')[0].value);
      popup.find('.favuser-comment')[0].value = favUserInfo.info;
//       console.log("z23 %o", popup.find('.favuser-comment')[0].value);

//       console.log("z31 %o", popup.find('.usergroups-select')[0]);
//       console.log("z32 %o", popup.find('.usergroups-select')[0].value);
			popup.find('.usergroups-select')[0].value = favUserInfo.group || 0;
//       console.log("z33 %o", popup.find('.usergroups-select')[0].value);

// 			console.log("client 000: %o", JSON.stringify(favUserInfo));

      // On submit, save user comment and group, if any
      popup.find('.popup-submit').click(function () {

// 	      console.log("z23 %o", popup.find('.favuser-comment')[0].value);
// 	      console.log("z33 %o", popup.find('.usergroups-select')[0].value);

        favUserInfo.info  = popup.find('.favuser-comment')[0].value;
        favUserInfo.group = popup.find('.usergroups-select')[0].value;
//         console.log("client call: %s", JSON.stringify(favUserInfo));
        
        saveData("", "", userName, { "detail": favUserInfo } );
        popup.fadeOutAndRemove();
      });

      
			popup.center();
			StackExchange.helpers.bindMovablePopups();
    }

    // Re-attach handlers (clear first if it existed before)
    $("#content").off( "click", ".favuser-info" );
		$("#content").on( "click", ".favuser-info", function( event ) {
			/** @type jQuery */
			var triggerElement = $( event.target );
      //alert("click");
			popup(triggerElement);
		});

    $(".comment").each(function (i, post) {
      //addSpinner(post);

      var userLink = $(post).find(".comment-user").first();
      var userName = $(userLink).clone().children().remove().end().text();
      //dalert(1);
      var userA = $(userLink)[0].href;
      if (!userA) return;
      var userId = userA.match(/\/users\/([-\d]+)\//)[1];
      //dalert(2);
      //dalert(userId +"/"+ userName +"/"+ userSite);

      var favUserInfo = getUserGroup(userSite, userId, userName);
      var newClassId = favUserInfo.group || 0;
      var newClassMaybe = (favUserInfo.certain < 9 ? "-maybe" : "");
      if (newClassId != 0) {
        var newClass = "userG" + newClassId + newClassMaybe;
        // Highlight comment
        $(post).find(".comment-actions").addClass(newClass);
      }
    })
  }

    // This would trigger re-highlightin on AJAX operations like expanding hidden comments
	$(document).ajaxComplete(function() {
    setTimeout(() => {
// 	    console.log("Running re-highlight");
  	  highlightPosts();
		}, 100);
  });
  

  // Prepare and register all styles
  var allStyle = "";
  for (var key in GROUPS) {
    allGroups += " userG" + key;
    var replacements = { "%number%": parseInt(key), "%color%": GROUPS[key].color };
    allStyle += STYLE_USERGROUP.replace(/%\w+%/g, function (placeholder) {
      return replacements[placeholder] || placeholder;
    });

  };
  //dalert("style: "+allStyle);
  // console.log(`style= ${allStyle}`);
  allStyle += STYLE_INFOPANEL;
  addGlobalStyle(allStyle);
  
  //alert("domain=" + document.domain);

  const VOTERS_REGEX = /\/users\?.*tab=Voters&filter=all/i;
  var thisUrl = window.location.href;
  if (VOTERS_REGEX.test(thisUrl)) {
    // alert("highlightPosts");
    highlightPosts();
  } else {
    // alert("highlightPosts");
    highlightPosts();
  }
/* */
	});
}
/* */
/*
})(window.eval("StackExchange"));
*/
})();
