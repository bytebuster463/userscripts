Userscripts
===========
Userscripts for Firefox (with Greasemonkey) and Chrome.

- [DreamWidth Reposter](https://bitbucket.org/bytebuster463/userscripts/src/master/dwreposter/): Adds LJ-like Repost functionality for DreamWidth
- [DreamWidth Localization for UA](https://bitbucket.org/bytebuster463/userscripts/src/master/dw.l10n.ua/): This is a localization script for DW to set for Ukrainian language
- [Semagic Dictionary for Ukrainian](https://bitbucket.org/bytebuster463/userscripts/src/master/Semagic Dictionary UA/): This is a spellcheck dictionaries for Ukrainian language to use with Semagic

Abandoned:
