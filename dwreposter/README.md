DreamWidth Reposter
===================

Adds LJ-like Repost functionality for DreamWidth

This plugin adds a "Repost" link under each DW-post
Clicking the link redirects to "Post New" page of DreamWidth and populates the Subject and Body fields.

It does **not** fill Journal/Community nor set Tags nor submit the post.

Refer [this article](http://bytebuster.dreamwidth.org/6480499.html) for instructions

Requirements
============

FireFox with Greasemonkey
