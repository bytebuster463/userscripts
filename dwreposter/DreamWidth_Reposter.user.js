// ==UserScript==
// @name        DreamWidth Reposter
// @namespace   bytebuster.dreamwidth.org
// @description Adds LJ-like Repost functionality for DreamWidth
// @require     https://www.cssscript.com/demo/custom-context-menus-vanilla-javascript/context-menu.js
// @include     /https?://.*\.dreamwidth\.org/.*/
// @exclude     /https?://.*\.dreamwidth\.org/(profile|calendar|tools|manage|customize).*/
// @exclude     /https?://.*\.dreamwidth\.org/tag/$/
// @include     /https?://.*\.livejournal\.com/.*/
// @exclude     /https?://.*\.livejournal\.com/(profile|calendar).*/
// @exclude     /https?://.*\.livejournal\.com/tag/$/
// @version     0.8
// @grant       GM.getValue
// @grant       GM.setValue
// @grant       GM.deleteValue
// ==/UserScript==

/* **********************************************************************************

    This script has two main functions:
    * addRepostLinks - triggers on all DreamWidth pages; adds "Repost" links to entries;
    * populatePostForm - triggers on /entry/new page; fills up the "Post Entry" fields

    Цей скрипт містить дві основні функції:
    * addRepostLinks - запускається на усіх сторінках DreamWidth pages; додає кнопку «Перепостити»;
    * populatePostForm - запускається на сторінці /entry/new page; заповнює поля вводу

********************************************************************************** */

/* ************* VERSION HISTORY ****************************************************
 * 0.8 2018/03/25
       added configurable post destinations
 * 0.7 2018/01/05
       added partial support for LJ
       minor improvements on processing cuttags
 * 0.6 Migrated to GM 4.0, async workout
 * 0.5 added conversion of <br> to \n
       fixed Youtube embeds for multiple reposts the same article
       fixed multiple Youtube frames in same post
       fixed .replaceChild since it has been broken in FF v.51
       fixed underscore "_" vs. dash "-" ambiguity
       fixed to work with old DW.Post mechanism
 * 0.4 added expanding cuttags
       added replacing cuttags in repost body
 * 0.3 fixed applied domain and URL;
       fixed Youtube video embed
       minor fix if mistakenly decided that post in community,
         while it actually is in user's own journal
 * 0.2 added group/blog references
       fix bug on other users references
 * 0.1 Initial reease
*/

/* ************* TODO ***************************************************************

* elRepostLink.addEventListener enforce no redirect if script fails
* add @site attribute into <user name="xxx" site=lj /> for reposts from LJ
  caveat: it would BREAK work with Semagic as Semagic erroneously REMOVES
  <user> elements having unrecognized tags
* fix lj-style cut tags, e.g. between <a name="cutid1"></a>CONTENT<a name="cutid1-end"></a>
* Feature: implement Shift+Click on Repost button to skip the popup menu and immediately navigate
  to Post page with a "default" set of repost options

********************************************************************************** */

(function(){
  // **** SETTINGS ****************************************************
  // Main settings
  // Головні налаштування
  const REPOST_TEXT = "Перепостити"; // Так виглядатиме текст кнопки «Репост»
  const REPOST_TEMPLATE = 'Перепост допису <user name="{poster}">: <a href="{link}">{subj}</a>\n{body}';
  const REPOST_TEMPLATE_COMMUNITY = 'Перепост допису <user name="{poster}"> @ <lj user="{community}">: <a href="{link}">{subj}</a>\n{body}';
  const DEBUG = false;

  // Here we manage the popup menu that appears on clicking the Repost button
  // Тут керуємо спливаючим меню, яке зʼявляється при натисканні на кнопку перепосту
  const DESTINATIONS = [
    {name: "UPW.News", journal: "ua_patriotic_war", icon: "0-News", tags: "bytebuster463, новини"},
    {name: "Lenta.News", journal: "lenta_ua", icon: "0-News", tags: "Війна, огляд дня, огляд преси, окупація, хроніки"},
    {name: "Bitter.News", journal: "bitter_onion", icon: "0-News", tags: "Україна, новини, bytebuster463"},
    {},
    {name: "UPW.Blogs", journal: "ua_patriotic_war", icon: "0-News", tags: "bytebuster463, новини-блогосфери"},
    {name: "Bitter.Blogs", journal: "bitter_onion", icon: "0-News", tags: "аналітика, Україна, bytebuster463"},
    {},
    {name: "Plain", journal: "", icon: "", tags: "", default: true},
  ];
  
  // **** CONSTANTS ***************************************************
  // You don't need it unless DreamWidth changes its classes (impossible, yeah)
  // Вам тут не треба нічого міняти, допоки Дрім не поміняє імена класів (майже нереально)

  var Mode = Object.freeze({
    HZ : 0,
    DW : 1,
    LJ : 2
  });
  const SITE_DW = "dreamwidth.org";
  const SITE_LJ = "livejournal.com";

	const QS_SUBJECT = "h3.entry-title, dt.entry-title h4, h1.entry-title";
	const QS_LINK = "h3.entry-title a, dt.entry-title a.subj-link";
  const QS_LINK_LJ = "link[rel='canonical']";
  const CLS_ENTRY_WRAPPER = ".entry-wrapper";
  const CLS_LJ_ENTRY_WRAPPER = ".entry-wrap, .post-asset, .b-singlepost";
  const CLS_ENTRY_POSTER  = "entry-poster";
  const CLS_LJUSER        = "ljuser";
  const CLS_LJUSERICON    = "user-icon";
  const ATTR_LJUSER       = "lj:user";
  const CLS_ENTRY_CONTENT = ".entry-content";
  const CLS_LJ_ENTRY_CONTENT = ".entry-content, .asset-content .asset-body";
  const CLS_ENTRY_LINKS   = ".entry-interaction-links";
  const CLS_LJ_ENTRY_LINKS   = ".asset-entry-links>.asset-meta-list, .b-linkbar, .entrysubmenu>ul, .entry-submenu";
  const ID_INPUT_SUBJECT  = "id-subject-0";
  const ID_INPUT_SUBJECT_OLD = "subject";
  const ID_INPUT_BODY     = "id-event-1";
  const ID_INPUT_BODY_OLD = "draft";
  const ID_INPUT_JOURNAL  = "js-usejournal";
  const ID_INPUT_JOURNAL_OLD = "ZZZZZ1";
  const ID_INPUT_ICON     = "js-icon-select";
  const ID_INPUT_ICON_OLD = "ZZZZZ2";
  const CLS_INPUT_TAGS     = "autocomplete-input"; //id"js-taglist";
  const CLS_INPUT_TAGS_OLD = "ZZZZZ3";
  const CLS_EMBED         = "lj_embedcontent-wrapper";
	const QS_LJSALE				  = ".ljsale, div.lj-like";
  const POST_URL = "http://www.dreamwidth.org/update";
  const POST_URL2 = "/entry/new";
  const REGEX_POST_URL = /\/entry\/new|\/update/
  const REPOST_PARAM="repost";
  const CONTENT_DEFAULT = "";
  const CONTENT_TAG = "DW_REPOST_CONTENT"; // how we save the reposted entry in GM_setValue

  const REGEX_YOUTUBE_LINK     =  /^.*(?:youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/gmi; // /www.youtube.com\/watch?v=(.+?)/gmi;
  const EMBED_YOUTUBE_TEMPLATE = '<iframe width="640" height="360" src="https://www.youtube.com/embed/{videoID}" frameborder="0" allowfullscreen></iframe>';

  // applies a parser on strings and log error if failed
  function parse(parser, where, description) {
    var ret;
    parser.lastIndex = 0; // reset the parser for multiple reuse
    match = parser.exec(where);
    if( match === null ) {
      console.log("Failed to parse " + description + "["+parser+"]" + " from text: ["+where+"]");
      ret = "(null)";
    } else {
      ret = match[1];
    }
    return ret;
  }

  function DebugMessage( what ) {
    if(DEBUG) { console.log(what); }
  }
  function dalert( what ) {
    if(DEBUG) { alert(what); }
  }

    // Detects if we're at DW or LJ
    function getMode() {
      var thisUrl = window.location.href;
      if(thisUrl.indexOf(SITE_DW) != -1) {
        return Mode.DW;
      } else if (thisUrl.indexOf(SITE_LJ) != -1) {
        return Mode.LJ;
      } else {
        return Mode.HZ;
      }
    }
  // checks for URL parameter
  function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
      var pair = vars[i].split("=");
      if (pair[0] == variable) {
        return pair[1];
      }
    }
    DebugMessage('Query Variable ' + variable + ' not found');
  }

  // Adds Repost links to all entries on the page
  // Додає посилання «Перепост» внизу усіх дописів на сторінці
  function addRepostLinks() {
    DebugMessage("Adding Repost links begin...");
    var entries = document.querySelectorAll(getMode()==Mode.DW ? CLS_ENTRY_WRAPPER : CLS_LJ_ENTRY_WRAPPER);

    DebugMessage("url="+window.location.href +"mode = " + getMode() + ", found entries =" + entries.length);
    for (var entry_index=0; entry_index<entries.length; entry_index++) {
      addRepostLink(entries[entry_index]);
    }

//    function foo(name1) {
//      return target => { console.log('Selected ' + name1, target.outerHTML); }
//    }
//    var foo = name1 => target => { console.log('Selected ' + name1, target.outerHTML); }
    
    var menuItems = [];

		for(var i in DESTINATIONS) {
    	var item = DESTINATIONS[i];

      if(item.name) {
        menuItems.push({
          "name"	: item.name,
			    // This curried function is here for capturing the current value of item
          // otherwise it would be always = last element in list, ref. "variable hoisting"
    			// ref. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures
          "fn"		: handleMenuItemSelect(item),
        });
      } else {
        menuItems.push( {} );
      }
		}
    
  	var cm1 = new ContextMenu('.dwr-popup', menuItems);
	  cm1.on('shown', () => console.log('Context menu shown'));
    
    
    DebugMessage("Adding Repost links end");
  }

  var handleMenuItemSelect = item => entry => {
    //dalert(
    console.log('Selected ' + JSON.stringify(item), entry.outerHTML);

    try {
      if( ! rememberPost(entry, item))
      { throw ("failed"); }
      window.open(POST_URL + "?" + REPOST_PARAM +"=1", "_blank");
    }
    catch (e) {
      alert("Error: " + e);
      event.stopPropagation(); event.preventDefault();
      throw (e);
    }
  }
  
	var handleClickLeft = entry => event => {
    DebugMessage(entry.outerHTML);
		let evt2 = new MouseEvent('contextmenu', {
			bubbles: true,
//      relatedTarget: entry,
			clientX: event.clientX, 
			clientY: event.clientY});
      
		entry.dispatchEvent(evt2);

		event.stopPropagation(); event.preventDefault();
	}

  // Adds Repost links to one entry
  // Додає посилання «Перепост» внизу допису
  function addRepostLink(entry) {
    var elFooter, elRepostLink, elRepostLinkLI; // elements
    var entryTitle, entryLink, userName, entryId;

    // check if entry is a DOM element
    // TODO obsolete? // if( !entry.tagName ) { return; }

    elFooter = entry.querySelectorAll(getMode()==Mode.DW ? CLS_ENTRY_LINKS : CLS_LJ_ENTRY_LINKS)[0];
		DebugMessage("elFooter=" + elFooter);
    DebugMessage("found footer=" + elFooter + ", it has elements:" + elFooter.outerHTML);
    // must be < ul >
    if(! ["UL", "IL"].includes(elFooter.tagName))
    	DebugMessage("Bogus footer " + elFooter.tagName +", errors possible.");

    elRepostLink = document.createElement("a");
    elRepostLink.href = POST_URL + "?" + REPOST_PARAM +"=1";
    elRepostLink.target = "_blank";
    elRepostLink.addEventListener('click', handleClickLeft(entry), false);
    elRepostLink.text = REPOST_TEXT;
    elRepostLinkLI = document.createElement("li");
    elRepostLink.className = "dwr-popup";
    entry.className += " dwr-popup";
    if(getMode()==Mode.LJ) {
      elRepostLinkLI.className += " asset-meta-comments item asset-meta-no-comments share";
    }
    elRepostLinkLI.appendChild(elRepostLink);
    elFooter.appendChild(elRepostLinkLI);
  }

  // Gets the saved content for repost
  async function getRepostData() {
    DebugMessage("getRepostData begin");

    return await GM.getValue(CONTENT_TAG, CONTENT_DEFAULT);
  }
  // Sets the content for repost
  function setRepostData(repostData) {
    DebugMessage("setRepostData begin " + repostData);
    // delete the state if it is the default
    if( repostData == CONTENT_DEFAULT ) {
      GM.deleteValue(CONTENT_TAG);
    } else {
      // set the state otherwise
      GM.setValue(CONTENT_TAG, repostData);
    }
    DebugMessage("setRepostData end");
  }

  // Fixes User references
  // Takes a cloned node to fix
  // Returns it as well
  function FixUsers(element) {
    var embeds; // list of embed objects

    DebugMessage("FixUsers begin");

    // LJ only: remove user icons
    embeds = element.getElementsByClassName(CLS_LJUSERICON);
    while(embeds.length > 0){
        embeds[0].parentNode.removeChild(embeds[0]);
    }

    // fix user references
    embeds = element.getElementsByClassName(CLS_LJUSER);

    for (var embed_index=0; embed_index<embeds.length; embed_index++) {
      var elLjUser, elLjUser2; // elements
      var userName; // strings

      elLjUser = embeds[embed_index];
      if(elLjUser.nodeName != "SPAN") { DebugMessage("Element is not SPAN so it does not look like a user reference, exiting"); continue; }

      userName = elLjUser.getAttribute(ATTR_LJUSER);
      if(! userName) { DebugMessage("Failed to find attribute [" +ATTR_LJUSER+ "], skipping"); continue; }
      // creating an HTML fragment to replace the broken link
      elLjUser2 = document.createElement("user");
      elLjUser2.setAttribute("name", userName);

      // replacing the element
      elLjUser.parentNode.replaceChild(elLjUser2, elLjUser);
      embed_index--;
    }
    DebugMessage("FixUsers end");
    return element;
  }

  // Fixes LJ Sale links
  // Takes a cloned node to fix
  // Returns it as well
  function FixLjSales(element) {
    var embeds; // list of embed objects

    DebugMessage("FixLjSales begin");

    element.querySelectorAll(QS_LJSALE).forEach (e =>
      e.parentNode.removeChild(e)
    );

    DebugMessage("FixLjSales end");
    return element;
  }

  // Fixes YouTube links
  // Takes a cloned node to fix
  // Returns it as well
  function FixYoutube(element) {
    var embeds; // list of embed objects

    DebugMessage("FixYoutube begin");

    embeds = element.getElementsByClassName(CLS_EMBED);

    for (var embed_index=0; embed_index<embeds.length; embed_index++) {
      var elFrame, elLinkDiv, elLink, elRange, elFragment, elFirstNode; // elements
      var linkAddress, linkBodyNew; // strings

      elFrame = embeds[embed_index];
      if(elFrame.nodeName != "DIV") { DebugMessage("Element frame does not look like a YouTube, exiting"); continue; }

      elLinkDiv = elFrame.nextSibling;
      if(elLinkDiv === null) { DebugMessage("Failed to find YouTube link DIV, exiting"); continue; }

      elLink = elLinkDiv.getElementsByTagName("a")[0];
      if(elLink === null) { DebugMessage("YouTube link seems to be broken, exiting"); continue; }

      linkAddress = parse(REGEX_YOUTUBE_LINK, elLink.href, "YouTube Link");
      linkBodyNew = EMBED_YOUTUBE_TEMPLATE
        .replace("{videoID}", linkAddress);

      // creating an HTML fragment to replace the broken link
      elRange = document.createRange();
      elRange.selectNode(document.body); // required in Safari
      elFragment = elRange.createContextualFragment(linkBodyNew);
      elFirstNode = elFragment.firstChild;

      // replacing the element
      elLinkDiv.parentNode.replaceChild(elFirstNode, elLinkDiv)
      elFrame.parentNode.removeChild(elFrame);
      embed_index--;
    }
    DebugMessage("FixYoutube end");
    return element;
  }

  // Fixes HTML elements representing cut tags,
  // replacing them with the newly created <lj-cut>
  function FixCutTags(elEntry) {
    var cuts; // collections

    // const arr = ['banana', 'monkey banana', 'apple', 'kiwi', 'orange'];
    //const checker = value =>
    //   !['banana', 'apple'].some(element => value.includes(element));

    // dalert(arr.filter(checker));

    cuts = elEntry.querySelectorAll(".cuttag_container");
    DebugMessage("cuttags#="+cuts.length);

    for(i=0; i<cuts.length; i++) {
      var elCut, elCutBody, elNewCut; // elements
      elCut = cuts[i];
      elCutBody = elCut.getElementsByTagName("div")[0];
      if ( ! elCutBody.classList.contains("cuttag-open")) {
        alert("Expand cut tags first!");
        return {result: false, element: null };
      }

      DebugMessage("elCutBody="+elCutBody.outerHTML);
      // Clean up trailing remainder of <cut>
      [].forEach.call(elCutBody.querySelectorAll("span a.cuttag-action"),
                      function(e) { DebugMessage(e.parentNode.outerHTML); e.parentNode.parentNode.removeChild(e.parentNode); }
                     );
      // re-create new <cut>
      elNewCut = document.createElement("cut");
      // move all child elements
      while (elCutBody.firstChild) {
          elNewCut.appendChild(elCutBody.firstChild);
      }
      // re-insert the newly created <cut>
      elCut.parentNode.replaceChild(elNewCut, elCut);
    }

    return { result: true, element: elEntry };
  }

  // Fixes underscore "_" to dash "-"
  function fixDash(s) {
    return s.replace(/_/g, "-");
  }

  // Saves JSON data from the reposted entry
  // Зберігає допис і його шапку і параметри перепосту
  function rememberPost(elEntry, options) {
    var elPoster, elLjuser, elTitle, elLink, elContent; // elements
    var entryPoster, entryCommunity, entryLink, entrySubject, entryBody, subdomain; // strings
    var repostData; // JSON object
    DebugMessage("rememberPost begin");

    subdomain = window.location.host.split(".")[0];
    // get Poster userId
    elPoster = elEntry.getElementsByClassName(CLS_ENTRY_POSTER)[0];

    if(elPoster && elPoster.hasChildNodes()) {
      // we seem to be in a community, parse out poster name from attribute
      elLjuser = elPoster.getElementsByClassName(CLS_LJUSER)[0];
      entryPoster = elLjuser.getAttribute(ATTR_LJUSER);
      entryCommunity = subdomain;
    } else {
      // we seem to be in a journal, parse out poster name from URL (FIXME)
      entryPoster = subdomain;
      entryCommunity = "";
    }

    // Quick fix: user or community name "_" -> "-"
    entryPoster = fixDash(entryPoster);
    entryCommunity = fixDash(entryCommunity);
    // dalert("entryPoster ="+entryPoster);
    // dalert("entryCommunity ="+entryCommunity);

    // Quick fix: fix if we mistakenly set community
    if(entryPoster == entryCommunity) {
      entryCommunity = "";
    }

    DebugMessage("elEntry = " + elEntry.outerHTML);

    // get Subj and Link
    DebugMessage("entry for Subject=" + elEntry.querySelector(QS_SUBJECT));
    entrySubject = elEntry.querySelector(QS_SUBJECT).textContent.trim();
    elLink   		 = elEntry.querySelector(QS_LINK);
    DebugMessage("elLink 1st=" + elLink);
    if(elLink === null) { elLink = document.querySelector(QS_LINK_LJ); }
    entryLink = elLink.href;
    
    DebugMessage(entryLink + " = " + entrySubject);

    // get Body
    elContent = elEntry.querySelectorAll(getMode()==Mode.DW ? CLS_ENTRY_CONTENT : CLS_LJ_ENTRY_CONTENT)[0]
      .cloneNode(true);
    // dalert(elContent);

    // Apply fixes
    // dalert("1 >>> " + elContent.outerHTML);
    var retCutTags = FixCutTags(elContent);
    if (! retCutTags.result) { return false; }
    elContent = retCutTags.element;
    //dalert("2 >>> " + elContent.outerHTML);

    elContent = FixYoutube(elContent);
    elContent = FixUsers(elContent);
    elContent = FixLjSales(elContent);

    entryBody = elContent.innerHTML
      .replace(/<br>/g, "\n");

    repostData =
      JSON.stringify(
        {
          "link"      : entryLink,
          "poster"    : entryPoster,
          "community" : entryCommunity,
          "subj"      : entrySubject,
          "body"      : entryBody,
          "options"		: options,
        }
      );

    DebugMessage("Saving JSON: " + repostData);
    setRepostData(repostData);
    DebugMessage("rememberPost end");
    return true;
  }

  // Fills up the "Post Entry" form
  // Заповнює форму «Створити допис»
  async function populatePostForm() {
    DebugMessage("Populate Post begin");

    var repostedUrl = getQueryVariable(REPOST_PARAM);
    if(! repostedUrl) {
      DebugMessage("Does not look like a repost, exiting");
      return;
    }

    // continue
    var repostDataString, repostData; // JSON data
    var postBody; // strings
    var elSubject, elBody, elJournal, elIcon, elTags; // elements
    var eventChange; // 'change' event dispatched to input elements

    repostDataString = await getRepostData();
    DebugMessage("Repost data=" + repostDataString);

    repostData = JSON.parse(repostDataString);
    DebugMessage("Repost data parsed=" + JSON.stringify(repostData));

    DebugMessage("Preparing postBody...");
    postBody =
      ((repostData.community == "") ? REPOST_TEMPLATE : REPOST_TEMPLATE_COMMUNITY)
      .replace('{poster}', repostData.poster)
      .replace('{community}', repostData.community)
      .replace('{link}', repostData.link)
      .replace('{subj}', repostData.subj)
      .replace('{body}', repostData.body)
    ;

    DebugMessage("postBody=" + postBody);

//  updated to work with old DW.Post mechanism
    elBody = document.getElementById(ID_INPUT_BODY);
    if(elBody === null) { elBody = document.getElementById(ID_INPUT_BODY_OLD);  }
    DebugMessage("elBody was: " + elBody.value);
    elBody.value = postBody;
    DebugMessage("elBody became: " + elBody.value);

    elSubject = document.getElementById(ID_INPUT_SUBJECT);
    if(elSubject === null) { elSubject = document.getElementById(ID_INPUT_SUBJECT_OLD); }
    DebugMessage("elSubject was: " + elSubject.value);
    elSubject.value = repostData.subj;
    DebugMessage("elSubject became: " + elSubject.value);

    elJournal = document.getElementById(ID_INPUT_JOURNAL);
    if(elJournal === null) { elJournal = document.getElementById(ID_INPUT_JOURNAL_OLD); }
    DebugMessage("elJournal was: " + elJournal.value);
    if(repostData.options.journal) {
      for(i=0; i<elJournal.length; i++) {
        if(fixDash(elJournal[i].value) == fixDash(repostData.options.journal)) {
          elJournal[i].selected = true;
          break;
        }
      	//elJournal.value = repostData.options.journal;
      }
    }
    elJournal.dispatchEvent(new Event('change'));
    DebugMessage("elJournal became: " + elJournal.value);

    elIcon = document.getElementById(ID_INPUT_ICON);
    if(elIcon === null) { elIcon = document.getElementById(ID_INPUT_ICON_OLD); }
    DebugMessage("elIcon was: " + elIcon.value);
    if(repostData.options.icon) { elIcon.value = repostData.options.icon; }
    elIcon.dispatchEvent(new Event('change'));
    DebugMessage("elIcon became: " + elIcon.value);

    elTags = document.getElementsByClassName(CLS_INPUT_TAGS)[0];
    if(elTags === null) { elTags = document.getElementsByClassName(CLS_INPUT_TAGS_OLD)[0]; }
    DebugMessage("elTags was: " + elTags.value);
    if(repostData.options.tags) { elTags.value = repostData.options.tags; }
    elTags.dispatchEvent(new Event('change'));

    DebugMessage("elTags became: " + elTags.value);

    setRepostData(CONTENT_DEFAULT);
    DebugMessage("Populate Post end");
  }

  var thisUrl = window.location.href;
  if(thisUrl.match(REGEX_POST_URL) !== null) {
//  if(thisUrl.indexOf(POST_URL2) != -1) {
    populatePostForm();
  } else {
    addRepostLinks();
  }
})();
